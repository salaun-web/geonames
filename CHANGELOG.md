# Changelog

All notable changes to `geonames` will be documented in this file.

## 1.0.0 - 2021-10-21

- formatting the package to Spatie/laravel-package-skeleton
- renamming geonames_geonames primary key from `geoname_id` to `id` following Laravel namming convention
- renamming geonames_alternate_names primary key from `alternate_name_id` to `id` following Laravel namming convention

## 0.1.5 - 2021-10-21

- initial fork of Yurtesen package


