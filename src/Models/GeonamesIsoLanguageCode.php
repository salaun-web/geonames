<?php

namespace Salaun\Geonames\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Salaun\Geonames\Models\GeonamesIsoLanguageCode
 *
 * @property string $iso_639_3
 * @property string $iso_639_2
 * @property string $iso_639_1
 * @property string $language_name
 * @method static \Illuminate\Database\Query\Builder|\Salaun\Geonames\Models\GeonamesIsoLanguageCode whereIso6393($value)
 * @method static \Illuminate\Database\Query\Builder|\Salaun\Geonames\Models\GeonamesIsoLanguageCode whereIso6392($value)
 * @method static \Illuminate\Database\Query\Builder|\Salaun\Geonames\Models\GeonamesIsoLanguageCode whereIso6391($value)
 * @method static \Illuminate\Database\Query\Builder|\Salaun\Geonames\Models\GeonamesIsoLanguageCode whereLanguageName($value)
 * @mixin \Eloquent
 */
class GeonamesIsoLanguageCode extends Model
{
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [];

	/**
	 * The attributes that should be hidden for arrays.
	 *
	 * @var array
	 */
	protected $hidden = [];

	/**
	 * The primary key for the model.
	 *
	 * @var string
	 */
	protected $primaryKey = 'iso_639_3';

	/**
	 * Indicates if the IDs are auto-incrementing.
	 *
	 * @var bool
	 */
	public $incrementing = false;

	/**
	 * Indicates if the model should be timestamped.
	 *
	 * @var bool
	 */
	public $timestamps = false;
}
