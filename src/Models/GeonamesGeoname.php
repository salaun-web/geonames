<?php

namespace Salaun\Geonames\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

/**
 * Salaun\Geonames\Models\GeonamesGeoname
 *
 * @property integer $geoname_id
 * @property string $name
 * @property string $ascii_name
 * @property string $alternate_names
 * @property float $latitude
 * @property float $longitude
 * @property string $feature_class
 * @property string $feature_code
 * @property string $country_code
 * @property string $cc2
 * @property string $admin1_code
 * @property string $admin2_code
 * @property string $admin3_code
 * @property string $admin4_code
 * @property integer $population
 * @property integer $elevation
 * @property integer $dem
 * @property string $timezone_id
 * @property string $modified_at
 * @property-read \Salaun\Geonames\Models\GeonamesAlternateName $alternateName
 * @property-read \Salaun\Geonames\Models\GeonamesTimezone $timeZone
 * @property-read \Salaun\Geonames\Models\GeonamesCountryInfo $countryInfo
 * @method static \Illuminate\Database\Query\Builder|\Salaun\Geonames\Models\GeonamesGeoname whereGeonameId($value)
 * @method static \Illuminate\Database\Query\Builder|\Salaun\Geonames\Models\GeonamesGeoname whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\Salaun\Geonames\Models\GeonamesGeoname whereAsciiName($value)
 * @method static \Illuminate\Database\Query\Builder|\Salaun\Geonames\Models\GeonamesGeoname whereAlternateNames($value)
 * @method static \Illuminate\Database\Query\Builder|\Salaun\Geonames\Models\GeonamesGeoname whereLatitude($value)
 * @method static \Illuminate\Database\Query\Builder|\Salaun\Geonames\Models\GeonamesGeoname whereLongitude($value)
 * @method static \Illuminate\Database\Query\Builder|\Salaun\Geonames\Models\GeonamesGeoname whereFeatureClass($value)
 * @method static \Illuminate\Database\Query\Builder|\Salaun\Geonames\Models\GeonamesGeoname whereFeatureCode($value)
 * @method static \Illuminate\Database\Query\Builder|\Salaun\Geonames\Models\GeonamesGeoname whereCountryCode($value)
 * @method static \Illuminate\Database\Query\Builder|\Salaun\Geonames\Models\GeonamesGeoname whereCc2($value)
 * @method static \Illuminate\Database\Query\Builder|\Salaun\Geonames\Models\GeonamesGeoname whereAdmin1Code($value)
 * @method static \Illuminate\Database\Query\Builder|\Salaun\Geonames\Models\GeonamesGeoname whereAdmin2Code($value)
 * @method static \Illuminate\Database\Query\Builder|\Salaun\Geonames\Models\GeonamesGeoname whereAdmin3Code($value)
 * @method static \Illuminate\Database\Query\Builder|\Salaun\Geonames\Models\GeonamesGeoname whereAdmin4Code($value)
 * @method static \Illuminate\Database\Query\Builder|\Salaun\Geonames\Models\GeonamesGeoname wherePopulation($value)
 * @method static \Illuminate\Database\Query\Builder|\Salaun\Geonames\Models\GeonamesGeoname whereElevation($value)
 * @method static \Illuminate\Database\Query\Builder|\Salaun\Geonames\Models\GeonamesGeoname whereDem($value)
 * @method static \Illuminate\Database\Query\Builder|\Salaun\Geonames\Models\GeonamesGeoname whereTimezoneId($value)
 * @method static \Illuminate\Database\Query\Builder|\Salaun\Geonames\Models\GeonamesGeoname whereModifiedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Salaun\Geonames\Models\GeonamesGeoname admin1()
 * @method static \Illuminate\Database\Query\Builder|\Salaun\Geonames\Models\GeonamesGeoname addCountryInfo()
 * @method static \Illuminate\Database\Query\Builder|\Salaun\Geonames\Models\GeonamesGeoname city($name = null, $featureCodes = array())
 * @method static \Illuminate\Database\Query\Builder|\Salaun\Geonames\Models\GeonamesGeoname country($name = null, $featureCodes = array())
 * @method static \Illuminate\Database\Query\Builder|\Salaun\Geonames\Models\GeonamesGeoname searchByFeature($name = null, $feature_class = null, $featureCodes = null)
 * @mixin \Eloquent
 */
class GeonamesGeoname extends Model
{
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [];

	/**
	 * The attributes that should be hidden for arrays.
	 *
	 * @var array
	 */
	protected $hidden = [];

	/**
	 * The primary key for the model.
	 *
	 * @var string
	 */
	protected $primaryKey = 'id';

	/**
	 * Indicates if the IDs are auto-incrementing.
	 *
	 * @var bool
	 */
	public $incrementing = false;

	/**
	 * Indicates if the model should be timestamped.
	 *
	 * @var bool
	 */
	public $timestamps = false;

	/**
	 *  Most useful fields which we want in every result which utilize our scopes
	 *
	 * @var array
	 */
	private $usefulScopeColumns = [
		'geonames_geonames.id',
		'geonames_geonames.name',
		'geonames_geonames.country_code'
	];

	/**
	 * One-to-One relation with GeonamesAlternateNames
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\HasOne
	 */
	public function alternateName()
	{
		return $this->hasMany(GeonamesAlternateName::class, 'geoname_id', 'id');
	}

	/**
	 * One-to-One relation with GeonamesTimezones
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\HasOne
	 */
	public function timeZone()
	{
		return $this->hasOne(GeonamesTimezone::class, 'timezone_id', 'timezone_id');
	}

	/**
	 * One-to-One relation with GeonamesCountryInfos
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\HasOne
	 */
	public function countryInfo()
	{
		return $this->hasOne(GeonamesCountryInfo::class, 'iso', 'country_code');
	}


	/**
	 * Return admin1 information in result
	 *
	 * @param \Illuminate\Database\Query\Builder $query
	 * @return \Illuminate\Database\Query\Builder
	 */
	public function scopeAdmin1($query)
	{
		$table = 'geonames_geonames';

		if (!isset($query->getQuery()->columns))
			$query = $query->addSelect($this->usefulScopeColumns);

		$query = $query
			->leftJoin(
				'geonames_admin1_codes as admin1',
				'admin1.code',
				'=',
				DB::raw('CONCAT_WS(\'.\',' .
					$table . '.country_code,' .
					$table . '.admin1_code)')
			)
			->addSelect(
				'admin1.geoname_id as admin1_geoname_id',
				'admin1.name as admin1_name'
			);

		return $query;
	}

	/**
	 * Return country information in result
	 *
	 * @param \Illuminate\Database\Query\Builder|\Illuminate\Database\Eloquent\Builder $query
	 * @return \Illuminate\Database\Query\Builder
	 */
	public function scopeAddCountryInfo($query)
	{
		$table = 'geonames_geonames';

		if (!isset($query->getQuery()->columns))
			$query = $query->addSelect($this->usefulScopeColumns);

		$query = $query
			->leftJoin(
				'geonames_country_infos as country_info',
				$table . '.country_code',
				'=',
				'country_info.iso'
			)
			->addSelect(
				'country_info.geoname_id as country_info_geoname_id',
				'country_info.country as country_info_country'
			);

		return $query;
	}


	/**
	 * Build a query to find major cities. Accepts wildcards eg. 'Helsin%'
	 *
	 * Suggested index for search:
	 * ALTER TABLE geonames_geonames ADD INDEX geonames_geonames_feature_name_index
	 *                                                          (`feature_class`,`feature_code`,`name`);
	 * and if you will limit queries by country you should also use:
	 * ALTER TABLE geonames_geonames ADD INDEX geonames_geonames_country_feature_name_index
	 *                                                          (`country_code`,`feature_class`,`feature_code`,`name`);
	 *
	 *
	 * @param \Illuminate\Database\Query\Builder|\Illuminate\Database\Eloquent\Builder $query
	 * @param String $name
	 * @param array $featureCodes List of feature codes to use when returning results
	 *                            defaults to ['PPLC','PPLA','PPLA2', 'PPLA3']
	 * @return \Illuminate\Database\Query\Builder
	 */
	public function scopeCity($query, $name = null, $featureCodes = ['PPLC', 'PPLA', 'PPLA2', 'PPLA3'])
	{
		return $this->scopeSearchByFeature($query, $name, 'P', $featureCodes);
	}

	/**
	 * Build a query to find major ccountries. Accepts wildcards eg. '%Finland%'
	 *
	 * Suggested index for search:
	 * ALTER TABLE geonames_geonames ADD INDEX geonames_geonames_feature_name_index
	 *                                                          (`feature_class`,`feature_code`,`name`);
	 *
	 * @param \Illuminate\Database\Query\Builder|\Illuminate\Database\Eloquent\Builder $query
	 * @param String $name
	 * @param array $featureCodes List of feature codes to use when returning results
	 *                            defaults to ['PCLI']
	 * @return \Illuminate\Database\Query\Builder
	 */
	public function scopeCountry($query, $name = null, $featureCodes = ['PCLI'])
	{
		return $this->scopeSearchByFeature($query, $name, 'A', $featureCodes);
	}


	/**
	 * Generic query used by scopes, but you can call it with custom feataure codes.
	 *
	 * @param \Illuminate\Database\Query\Builder|\Illuminate\Database\Eloquent\Builder $query
	 * @param String $name
	 * @param String $feature_class The 1 character feature class
	 * @param array $featureCodes List of feature codes to use when returning results
	 * @return \Illuminate\Database\Query\Builder
	 */
	public function scopeSearchByFeature($query, $name = null, $feature_class = null, $featureCodes = null)
	{
		$table = 'geonames_geonames';

		if (!isset($query->getQuery()->columns))
			$query = $query->addSelect($this->usefulScopeColumns);

		if ($name !== null)
			$query = $query->where($table . '.name', 'LIKE', $name);

		$query = $query
			->where($table . '.feature_class', $feature_class)
			->whereIn($table . '.feature_code', $featureCodes);

		return $query;
	}
}
