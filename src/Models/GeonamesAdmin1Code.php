<?php

namespace Salaun\Geonames\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Salaun\Geonames\Models\GeonamesAdmin1Code
 *
 * @property string $code
 * @property string $name
 * @property string $name_ascii
 * @property integer $geoname_id
 * @property-read \Salaun\Geonames\Models\GeonamesGeoname $geoname
 * @property-read \Illuminate\Database\Eloquent\Collection|\Salaun\Geonames\Models\GeonamesHierarchy[] $hierarchies
 * @method static \Illuminate\Database\Query\Builder|\Salaun\Geonames\Models\GeonamesAdmin1Code whereCode($value)
 * @method static \Illuminate\Database\Query\Builder|\Salaun\Geonames\Models\GeonamesAdmin1Code whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\Salaun\Geonames\Models\GeonamesAdmin1Code whereNameAscii($value)
 * @method static \Illuminate\Database\Query\Builder|\Salaun\Geonames\Models\GeonamesAdmin1Code whereGeonameId($value)
 * @mixin \Eloquent
 */
class GeonamesAdmin1Code extends Model
{
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [];

	/**
	 * The attributes that should be hidden for arrays.
	 *
	 * @var array
	 */
	protected $hidden = [];

	/**
	 * The primary key for the model.
	 *
	 * @var string
	 */
	protected $primaryKey = 'geoname_id';

	/**
	 * Indicates if the IDs are auto-incrementing.
	 *
	 * @var bool
	 */
	public $incrementing = false;

	/**
	 * Indicates if the model should be timestamped.
	 *
	 * @var bool
	 */
	public $timestamps = false;

	/**
	 * One-to-One relation with GeonamesGeonames
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\HasOne
	 */
	public function geoname()
	{
		return $this->hasOne(GeonamesGeoname::class, 'id', 'geoname_id');
	}

	/**
	 * One-to-Many relation with GeonamesGeonames
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\HasOne
	 */
	public function hierarchies()
	{
		return $this->hasMany(GeonamesHierarchy::class, 'parent_id', 'geoname_id');
	}
}
