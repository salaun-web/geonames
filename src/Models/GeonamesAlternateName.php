<?php

namespace Salaun\Geonames\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Salaun\Geonames\Models\GeonamesAlternateName
 *
 * @property integer $id
 * @property integer $geoname_id
 * @property string $iso_language
 * @property string $alternate_name
 * @property boolean $isPreferredName
 * @property boolean $isShortName
 * @property boolean $isColloquial
 * @property boolean $isHistoric
 * @property-read \Salaun\Geonames\Models\GeonamesGeoname $geoname
 * @method static \Illuminate\Database\Query\Builder|\Salaun\Geonames\Models\GeonamesAlternateName whereAlternateNameId($value)
 * @method static \Illuminate\Database\Query\Builder|\Salaun\Geonames\Models\GeonamesAlternateName whereGeonameId($value)
 * @method static \Illuminate\Database\Query\Builder|\Salaun\Geonames\Models\GeonamesAlternateName whereIsoLanguage($value)
 * @method static \Illuminate\Database\Query\Builder|\Salaun\Geonames\Models\GeonamesAlternateName whereAlternateName($value)
 * @method static \Illuminate\Database\Query\Builder|\Salaun\Geonames\Models\GeonamesAlternateName whereIsPreferredName($value)
 * @method static \Illuminate\Database\Query\Builder|\Salaun\Geonames\Models\GeonamesAlternateName whereIsShortName($value)
 * @method static \Illuminate\Database\Query\Builder|\Salaun\Geonames\Models\GeonamesAlternateName whereIsColloquial($value)
 * @method static \Illuminate\Database\Query\Builder|\Salaun\Geonames\Models\GeonamesAlternateName whereIsHistoric($value)
 * @mixin \Eloquent
 */
class GeonamesAlternateName extends Model
{
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [];

	/**
	 * The attributes that should be hidden for arrays.
	 *
	 * @var array
	 */
	protected $hidden = [];

	/**
	 * The primary key for the model.
	 *
	 * @var string
	 */
	protected $primaryKey = 'id';

	/**
	 * Indicates if the IDs are auto-incrementing.
	 *
	 * @var bool
	 */
	public $incrementing = false;

	/**
	 * Indicates if the model should be timestamped.
	 *
	 * @var bool
	 */
	public $timestamps = false;

	/**
	 * One-to-One relation with GeonamesGeoname
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function geoname()
	{
		return $this->belongsTo(GeonamesGeoname::class, 'id', 'geoname_id');
	}
}
