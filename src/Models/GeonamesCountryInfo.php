<?php

namespace Salaun\Geonames\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Salaun\Geonames\Models\GeonamesCountryInfo
 *
 * @property string $iso
 * @property string $iso3
 * @property string $iso_numeric
 * @property string $fips
 * @property string $country
 * @property string $capital
 * @property integer $area
 * @property integer $population
 * @property string $continent_code
 * @property integer $continent_id
 * @property string $tld
 * @property string $currency_code
 * @property string $currency_name
 * @property string $phone
 * @property string $postal_code_format
 * @property string $postal_code_regex
 * @property string $languages
 * @property integer $geoname_id
 * @property string $neighbors
 * @property string $equivalent_fips_code
 * @property-read \Salaun\Geonames\Models\GeonamesTimezone $timezone
 * @property-read \Salaun\Geonames\Models\GeonamesGeoname $continent
 * @method static \Illuminate\Database\Query\Builder|\Salaun\Geonames\Models\GeonamesCountryInfo whereIso($value)
 * @method static \Illuminate\Database\Query\Builder|\Salaun\Geonames\Models\GeonamesCountryInfo whereIso3($value)
 * @method static \Illuminate\Database\Query\Builder|\Salaun\Geonames\Models\GeonamesCountryInfo whereIsoNumeric($value)
 * @method static \Illuminate\Database\Query\Builder|\Salaun\Geonames\Models\GeonamesCountryInfo whereFips($value)
 * @method static \Illuminate\Database\Query\Builder|\Salaun\Geonames\Models\GeonamesCountryInfo whereCountry($value)
 * @method static \Illuminate\Database\Query\Builder|\Salaun\Geonames\Models\GeonamesCountryInfo whereCapital($value)
 * @method static \Illuminate\Database\Query\Builder|\Salaun\Geonames\Models\GeonamesCountryInfo whereArea($value)
 * @method static \Illuminate\Database\Query\Builder|\Salaun\Geonames\Models\GeonamesCountryInfo wherePopulation($value)
 * @method static \Illuminate\Database\Query\Builder|\Salaun\Geonames\Models\GeonamesCountryInfo whereContinentCode($value)
 * @method static \Illuminate\Database\Query\Builder|\Salaun\Geonames\Models\GeonamesCountryInfo whereContinentId($value)
 * @method static \Illuminate\Database\Query\Builder|\Salaun\Geonames\Models\GeonamesCountryInfo whereTld($value)
 * @method static \Illuminate\Database\Query\Builder|\Salaun\Geonames\Models\GeonamesCountryInfo whereCurrencyCode($value)
 * @method static \Illuminate\Database\Query\Builder|\Salaun\Geonames\Models\GeonamesCountryInfo whereCurrencyName($value)
 * @method static \Illuminate\Database\Query\Builder|\Salaun\Geonames\Models\GeonamesCountryInfo wherePhone($value)
 * @method static \Illuminate\Database\Query\Builder|\Salaun\Geonames\Models\GeonamesCountryInfo wherePostalCodeFormat($value)
 * @method static \Illuminate\Database\Query\Builder|\Salaun\Geonames\Models\GeonamesCountryInfo wherePostalCodeRegex($value)
 * @method static \Illuminate\Database\Query\Builder|\Salaun\Geonames\Models\GeonamesCountryInfo whereLanguages($value)
 * @method static \Illuminate\Database\Query\Builder|\Salaun\Geonames\Models\GeonamesCountryInfo whereGeonameId($value)
 * @method static \Illuminate\Database\Query\Builder|\Salaun\Geonames\Models\GeonamesCountryInfo whereNeighbors($value)
 * @method static \Illuminate\Database\Query\Builder|\Salaun\Geonames\Models\GeonamesCountryInfo whereEquivalentFipsCode($value)
 * @mixin \Eloquent
 */
class GeonamesCountryInfo extends Model
{
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [];

	/**
	 * The attributes that should be hidden for arrays.
	 *
	 * @var array
	 */
	protected $hidden = [];

	/**
	 * The primary key for the model.
	 *
	 * @var string
	 */
	protected $primaryKey = 'iso';

	/**
	 * Indicates if the IDs are auto-incrementing.
	 *
	 * @var bool
	 */
	public $incrementing = false;

	/**
	 * Indicates if the model should be timestamped.
	 *
	 * @var bool
	 */
	public $timestamps = false;

	/**
	 * One-to-One relation with GeonamesTimezone
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\HasOne
	 */
	public function timezone()
	{
		return $this->hasOne(GeonamesTimezone::class, 'country_code');
	}

	/**
	 * One-to-One relation with GeonamesGeonames
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\HasOne
	 */
	public function continent()
	{
		return $this->hasOne(GeonamesGeoname::class, 'id', 'continent_id');
	}
}
