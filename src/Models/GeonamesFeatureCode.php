<?php

namespace Salaun\Geonames\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Salaun\Geonames\Models\GeonamesFeatureCode
 *
 * @property string $code
 * @property string $name
 * @property string $description
 * @method static \Illuminate\Database\Query\Builder|\Salaun\Geonames\Models\GeonamesFeatureCode whereCode($value)
 * @method static \Illuminate\Database\Query\Builder|\Salaun\Geonames\Models\GeonamesFeatureCode whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\Salaun\Geonames\Models\GeonamesFeatureCode whereDescription($value)
 * @mixin \Eloquent
 */
class GeonamesFeatureCode extends Model
{
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [];

	/**
	 * The attributes that should be hidden for arrays.
	 *
	 * @var array
	 */
	protected $hidden = [];

	/**
	 * The primary key for the model.
	 *
	 * @var string
	 */
	protected $primaryKey = 'code';

	/**
	 * Indicates if the IDs are auto-incrementing.
	 *
	 * @var bool
	 */
	public $incrementing = false;

	/**
	 * Indicates if the model should be timestamped.
	 *
	 * @var bool
	 */
	public $timestamps = false;
}
