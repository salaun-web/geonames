<?php

namespace Salaun\Geonames\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Salaun\Geonames\Models\GeonamesTimezone
 *
 * @property string $timezone_id
 * @property string $country_code
 * @property float $gmt_offset
 * @property float $dst_offset
 * @property float $raw_offset
 * @method static \Illuminate\Database\Query\Builder|\Salaun\Geonames\Models\GeonamesTimezone whereTimezoneId($value)
 * @method static \Illuminate\Database\Query\Builder|\Salaun\Geonames\Models\GeonamesTimezone whereCountryCode($value)
 * @method static \Illuminate\Database\Query\Builder|\Salaun\Geonames\Models\GeonamesTimezone whereGmtOffset($value)
 * @method static \Illuminate\Database\Query\Builder|\Salaun\Geonames\Models\GeonamesTimezone whereDstOffset($value)
 * @method static \Illuminate\Database\Query\Builder|\Salaun\Geonames\Models\GeonamesTimezone whereRawOffset($value)
 * @mixin \Eloquent
 */
class GeonamesTimezone extends Model
{
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [];

	/**
	 * The attributes that should be hidden for arrays.
	 *
	 * @var array
	 */
	protected $hidden = [];

	/**
	 * The primary key for the model.
	 *
	 * @var string
	 */
	protected $primaryKey = 'timezone_id';

	/**
	 * Indicates if the IDs are auto-incrementing.
	 *
	 * @var bool
	 */
	public $incrementing = false;

	/**
	 * Indicates if the model should be timestamped.
	 *
	 * @var bool
	 */
	public $timestamps = false;
}
