<?php

namespace Salaun\Geonames\Models;

use Illuminate\Database\Eloquent\Relations\Pivot;

/**
 * Salaun\Geonames\Models\GeonamesHierarchy
 *
 * @property integer $parent_id
 * @property integer $child_id
 * @property string $type
 * @method static \Illuminate\Database\Query\Builder|\Salaun\Geonames\Models\GeonamesHierarchy whereParentId($value)
 * @method static \Illuminate\Database\Query\Builder|\Salaun\Geonames\Models\GeonamesHierarchy whereChildId($value)
 * @method static \Illuminate\Database\Query\Builder|\Salaun\Geonames\Models\GeonamesHierarchy whereType($value)
 * @mixin \Eloquent
 */
class GeonamesHierarchy extends Pivot
{
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [];

	/**
	 * The attributes that should be hidden for arrays.
	 *
	 * @var array
	 */
	protected $hidden = [];

	/**
	 * The primary key for the model.
	 *
	 * @var string
	 */
	protected $primaryKey = 'parent_id';

	/**
	 * Indicates if the model should be timestamped.
	 *
	 * @var bool
	 */
	public $timestamps = false;
}
