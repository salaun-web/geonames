<?php

namespace Salaun\Geonames;

use Salaun\Geonames\Commands\DownloadCommand;
use Spatie\LaravelPackageTools\Package;
use Spatie\LaravelPackageTools\PackageServiceProvider;
use Salaun\Geonames\Commands\InstallCommand;
use Salaun\Geonames\Commands\SeedCommand;

class GeonamesServiceProvider extends PackageServiceProvider
{
	public function configurePackage(Package $package): void
	{
		/*
         * This class is a Package Service Provider
         *
         * More info: https://github.com/spatie/laravel-package-tools
         */
		$package
			->name('geonames')
			->hasConfigFile()
			->hasMigrations([
				// Reference tables
				'create_geonames_feature_codes_table',
				'create_geonames_timezones_table',
				'create_geonames_iso_language_codes_table',
				// Main table
				'create_geonames_geonames_table',
				// Related tables
				'create_geonames_alternate_names_table',
				'create_geonames_country_infos_table',
				'create_geonames_admin1_codes_table',
				'create_geonames_admin2_codes_table',
				// Pivot tables
				'create_geonames_hierarchies_table',
			])
			->hasCommands([
				DownloadCommand::class,
				InstallCommand::class,
				SeedCommand::class,
			]);
	}
}
