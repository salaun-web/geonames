<?php

namespace Salaun\Geonames\Commands;

use Illuminate\Console\Command;

class DownloadCommand extends Command
{
	use CommandTrait;

	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'geonames:download
                            {--update : Updates the downloaded files to latest versions}
                           ';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Download geonames database txt/zip files';

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
		$update = $this->input->getOption('update');
		$this->downloadAllFiles($update);
	}
}
