<?php

namespace Salaun\Geonames\Commands;

use RuntimeException;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Schema;

class SeedCommand extends Command
{
	/**
	 * This includes some common functions used by commands
	 */
	use CommandTrait;

	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'geonames:seed
                            {--refresh : Truncate tables and re-insert data from scratch}
                            {--update-files : Update geonames files before inserting data to database}
                            {--table= : Only import the given database table}
                           ';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Seed the database from geonames files';

	/**
	 * Execute the console command.
	 *
	 * @throws RuntimeException;
	 * @return mixed
	 */
	public function handle()
	{
		$updateFiles = $this->input->getOption('update-files');
		$refresh = $this->input->getOption('refresh');
		$table = $this->input->getOption('table');

		if (isset($table)) {
			foreach ($this->files as $name => $file) {
				if ($file['table'] == $table) {
					$this->downloadFile($name, $updateFiles);
					$this->parseGeonamesText($name, $refresh);
					return;
				}
			}
			$this->line('<error>Table Not Found: </error> Table ' . $table . 'not found in configuration');
			return;
		} else {
			$this->downloadAllFiles($updateFiles);
			// Check if we have all the tables
			foreach ($this->getFilesArray() as $name => $file) {
				if (Schema::hasTable($file['table'])) {
					$this->parseGeonamesText($name, $refresh);
				} else {
					throw new RuntimeException($file['table'] . ' table not found. Did you run geoname:install then run migrate ?');
				}
			}
		}
		$this->line('<info>Finished : </info> Requested actions has been completed!');
	}
}
