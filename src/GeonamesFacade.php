<?php

namespace Salaun\Geonames;

use Illuminate\Support\Facades\Facade;

/**
 * @see \Salaun\Geonames\Geonames
 */
class GeonamesFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'geonames';
    }
}
