<?php

use Salaun\Geonames\Geonames;

beforeAll(fn() => Geonames::create([
	'id' => 13,
	'name' => 'Mars',
	'ascii_name' => 'mars',
	'alternate_names' => 'free planet',
	'latitude' => '0',
	'longitude' => '133',
	'feature_class' => '133',
]));

test('geonames to alternate names')->expect(fn() => Geonames::with()->)
