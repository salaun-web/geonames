# Geonames importer and models for Eloquent ORM. This repo is a fork from Yurtesen/Geonames.

[![License](https://poser.pugx.org/yurtesen/geonames/license.svg)](https://packagist.org/packages/yurtesen/geonames)

This package provides a way to import/update a local copy of [GeoNames](http://www.geonames.org/) databases.
The data is retrieved from GeoNames text file export on a [public server](https://download.geonames.org/export/dump/).
You are welcome to read GeoNames [README](https://download.geonames.org/export/dump/readme.txt) for more informations on this service.

## Installation

*Note:* If you are using Lumen. You have to first install [irazasyed/larasupport](https://github.com/irazasyed/larasupport) !

You can install the package via composer:

```bash
composer require salaun/geonames
```

You can publish and run the migrations with:

```bash
php artisan vendor:publish --provider="Salaun\Geonames\GeonamesServiceProvider" --tag="geonames-migrations"
php artisan migrate
```

You can publish the config file with:
```bash
php artisan vendor:publish --provider="Salaun\Geonames\GeonamesServiceProvider" --tag="geonames-config"
```

This is the contents of the published config file:

```php
return [
];
```

## Usage

Please see the [wiki](https://github.com/yurtesen/geonames/wiki) for further information.

## Provided Eloquent Models

Please see the *Wiki* pages for implementation details.

| Name                  | Key         | Relations                       | Scopes                  |
| --------------------- | ----------- | ------------------------------- | ----------------------- |
| GeonamesGeoname       | geoname_id  | alternateName, timeZone,country | admin1,city,countryInfo |
| GeonamesAlternateName | geoname_id  | geoname                         |                         |
| GeonamesCountryInfo   | iso         | timezone,continent              |                         |
| GeonamesFeatureCode   | code        |                                 |                         |
| GeonamesLanguageCode  | iso_639_3   |                                 |                         |
| GeonamesTimezone      | timezone_id |                                 |                         |
| GeonamesHierarchy     | parent_id   |                                 |                         |
| GeonamesAdmin1Code    | geoname_id  | geoname,hierarchies             |                         |
| GeonamesAdmin2Code    | geoname_id  | geoname,hierarchies             |                         |

## Tables
GeoNames file names and corresponding table names created in your database.

| Filename              | Tablename                |
| --------------------- | ------------------------ |
| timeZones.txt         | geonames_timezones       |
| allCountries.zip      | geonames_geonames        |
| countryInfo.txt       | geonames_country_infos   |
| iso-languagecodes.txt | geonames_language_codes  |
| alternateNames.zip    | geonames_alternate_names |
| hierarchy.zip         | geonames_hierarchies     |
| admin1CodesASCII.txt  | geonames_admin1_codes    |
| admin2Codes.txt       | geonames_admin2_codes    |
| featureCodes_en.txt   | geonames_feature_codes   |


## Testing

```bash
composer test
```

## Changelog

Please see [CHANGELOG](CHANGELOG.md) for more information on what has changed recently.

## Credits

- [Evren YURTESEN](https://github.com/yurtesen)
- [Quentin JAMELOT](https://github.com/salaun)

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.
